// Off Canvas
const offCanvas = document.getElementById("off-canvas");
const offCanvasMenuToggle = document.getElementById("ocm-toggle");
const offCanvasDismissBtn = document.getElementById("off-canvas-dismiss-btn");

const isOffCanvasVisible = offCanvas.style.opacity === 1;

function toggleOffCanvas(status) {
  requestAnimationFrame(function () {
    offCanvas.style.opacity = status;
    offCanvas.style.pointerEvents = status === 0 ? "none" : "auto";
  });
}

function openOffCanvas() {
  toggleOffCanvas(1);
}

function dismissOffCanvas() {
  toggleOffCanvas(0);
  togglePopUp(1);
}

offCanvasMenuToggle.addEventListener("click", openOffCanvas);
offCanvasDismissBtn.addEventListener("click", dismissOffCanvas);

// Popup
const popUp = document.getElementById("popup");
const popUpDismissBtn = document.getElementById("popup-dismiss-btn");

const isPopUpVisible = popUp.style.opacity === 1;

function togglePopUp(status) {
  requestAnimationFrame(function () {
    popUp.style.opacity = status;
    popUp.style.pointerEvents = status === 0 ? "none" : "auto";
  });
}

function openPopUp() {
  togglePopUp(1);
}

function dismissPopUp() {
  togglePopUp(0);
}

popUpDismissBtn.addEventListener("click", dismissPopUp);

document.addEventListener(
  "keydown",
  function (event) {
    if (["Esc", "Escape"].includes(event.key)) {
      dismissPopUp();
    }
  },
  false
);
