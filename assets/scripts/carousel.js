function computeSlidePositionQualifier(slidePosition) {
  let qualifier;

  switch (slidePosition) {
    case 1:
      qualifier = "first";
      break;
    case 2:
      qualifier = "second";
      break;
    case 3:
      qualifier = "last";
      break;
  }

  return qualifier;
}

if (document.readyState === "loading") {
  document.addEventListener("DOMContentLoaded", function () {
    new Swiper(".swiper", {
      pagination: {
        el: ".swiper-pagination",
        bulletClass: "page-dot",
        bulletActiveClass: "active",
        clickable: true,
        renderBullet: function (index, className) {
          return `<button class="${className}" type="button" aria-label="Go to the ${computeSlidePositionQualifier(
            index
          )} slide"/>`;
        },
      },
      keyboard: {
        enabled: true,
      },
    });
  });
}
