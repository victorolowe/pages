# Pages

There are two pages:

- Carousel (implemented with [SwiperJS](https://swiperjs.com/swiper-api))
- Promotions

The `index.html` view renders links to respective pages.

## Usage

You can

- View `index.html` directly in your browser OR
- Run `npx http-server` to host this webpage on a local server.
